'use strict'

const _ = require('lodash')
const pino = require('pino')
const path = require('path')

global.appRoot = path.resolve(__dirname)

const Application = require('./app')
const Resources = require('./resources')

const logger = pino({
  prettyPrint: {
    levelFirst: true
  },
  prettifier: require('pino-pretty')
})

const resources = new Resources(logger)

async function initResourced() {
  let environment = {
    db: await resources.db.bootstrap(),
    httpServer: await resources.httpServer.bootstrap()
  }

  return environment
}

module.exports = async function () {
  const environment = await initResourced()
  await resources.httpServer.listen()

  const app = new Application(environment, logger)  
  environment.model = await app.bootstrap()

  logger.info(`App running on port ${process.env.PORT}`)

  return environment
}()

