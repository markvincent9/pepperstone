'use strict'

const Db = require('./db')
const HttpServer = require('./http-server')

class Resources {
  constructor (logger) {
    this.db = new Db(logger)
    this.httpServer = new HttpServer(logger)
  }
}

module.exports = Resources