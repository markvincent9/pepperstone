'use strict'

const _ = require('lodash')
const cors = require('cors')
const express = require('express')
const expressPinoLogger = require('express-pino-logger')

class HttpServer {
  constructor(logger) {
    this.logger = logger
  }

  async bootstrap () {
    this.logger.info('Creating Http Server')

    let server = express()

    if (process.env.NODE_ENV !== 'test') {
      server.use(expressPinoLogger({
        logger: this.logger
      }))
    }
    
    server.use(cors())

    server.set('port', process.env.PORT || 8080)

    this.server = server
    return server
  }

  async listen () {
    this.server.listen(process.env.PORT, (err) => {
      if (!_.isNil(err)) throw new Error(`Error listening to Http Server due to: ${err.message}`)
      return
    })
  }

  async close () {
    this.logger.info('Closing Http Server')
  }
}

module.exports = HttpServer