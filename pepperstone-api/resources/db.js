'use strict'

const _ = require('lodash')
const mongoose = require('mongoose')

class Db {
  constructor (logger) {
    this.logger = logger
  }

  bootstrap () {
    const options = {
      reconnectTries: Number.MAX_VALUE,
      keepAlive: 10000,
      promiseLibrary: Promise,
      useNewUrlParser: true
    }

    this.logger.info('Connecting to system database...')

    const connection = mongoose.connect(process.env.MONGO_URL, options)
    connection.then(() => {
      this.logger.info('Connected to system database.')
    }).catch(err => {
      this.logger.error(`Error Connecting to Database due to: ${err.message}`)
    })

    return connection
  }

}

module.exports = Db
