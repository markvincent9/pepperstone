'use strict'

const fs = require('fs')
const path = require('path')

const should = require('should')
const request = require('supertest')


describe(`Api Test`, function () {
  let test = {}

  before(function (done) {
    this.timeout(5000)
    
    require('../main')
      .then(environment => {
        test = {
          api: environment.httpServer,
          model: environment.model
        }
        done()
      })
  })

  // delete all transaction and pre-populated accounts
  after(function (done) {
    async function deleteDocs () {
      await test.model.account.deleteMany({})
      await test.model.transaction.deleteMany({})
    }
    
    deleteDocs().then(done)
  })

  describe(`POST /transaction`, function () {
    it(`Should upload csv file and save it to MongoDB`, function (done) {
      this.timeout(5000)
      request(test.api)
        .post('/transactions')
        .field('type', 'transfer')
        .attach('file', 'tests/csv-files/test1.csv')
        .set({
          'content-type': 'multipart/form-data'
        })
        .expect(201, (err, res) => {
          should.ifError(err)
          should.exist(res.body._id, 'Transaction id is missing')
          test.id = res.body._id
          done()
        }) 
    })

    it(`Should validate invalid file`, function (done) {
      this.timeout(5000)
      request(test.api)
        .post('/transactions')
        .field('type', 'transfer')
        .attach('file', 'tests/csv-files/invalidFile.png')
        .expect(400, done)
    })

    it(`Should validate invalid type`, function (done) {
      this.timeout(5000)
      request(test.api)
        .post('/transactions')
        .field('type', 'transfersss')
        .attach('file', 'tests/csv-files/invalidFile.png')
        .expect(400, done)
    })

    it(`Should Get All Transactions`, function (done) {
      this.timeout(5000)
      request(test.api)
        .get('/transactions')
        .expect(200, (err, res) => {
          should.ifError(err)
          should(res.body).be.an.Array()
          should(res.body).not.be.empty()
          done()
        })
    })

    it(`Should test scheduler`, function (done) {
      this.timeout(20000)

      async function testProcess() {
        try {
          let fromAccount = await test.model.account.findOne({account: 198000})
          let toAccount = await test.model.account.findOne({account: 123599})
      
          await should.equal(fromAccount.balance, 999970 - 30, 'Cash Transfer did not work as expected')
          await should.equal(toAccount.balance, 1000000 + 30, 'Cash Transfer did not work as expected')
          done()
        } catch (err) {
          done()
        }
      }
      
      setTimeout(testProcess, 10000)
    })

    it(`Should check response file`, function (done) {
      this.timeout(20000)

      async function test() {
        try {
          let transaction = await test.model.transaction.findOne({_id: test.id})
          
          should.exist(transaction.responseFileId, 'Response file id does not exist')
          should.exist(transaction.processedDate, 'Processed Date does not exist')
          done()
        } catch (err) {
          done()
        }
      }
      
      test()
    })
  })
})

