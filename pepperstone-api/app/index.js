'use strict'

const Api = require('./api')
const Util = require('./util')
const Model = require('./model')
const Services = require('./services')
const FileScheduler = require('./file-scheduler')

class Application {
  constructor(env, logger) {
    this.services = new Services(logger)

    this.model = new Model()
    this.api = new Api(env.httpServer, this.services, new Util())
    this.fileScheduler = new FileScheduler(this.services)
  }

  async bootstrap () {
    this.model = await this.model.register()
    await this.api.bootstrap(this.model)
    await this.fileScheduler.bootstrap(this.model)
    return this.model
  }
}

module.exports = Application