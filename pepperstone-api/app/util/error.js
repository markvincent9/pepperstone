'use strict'

const _ = require('lodash')

// Request error handler
class ErrorUtil {
  constructor (logger) {
    this.logger = logger

    this.errors = {
      '400': {
        status: 400,
        code: 'InvalidContentError',
        message: 'Request validation has failed.',
        errors: []
      },
      '404': {
        status: 404,
        code: 'ResourceNotFound',
        message: 'Requested resourced is not available',
        errors: []
      },
      '500': {
        status: 500,
        code: 'InternalServerError',
        message: 'An unexpected error has occurred.',
        errors: []
      },
      '501': {
        status: 501,
        code: 'NotImplemented',
        message: 'Process not yet supported. Please contact your administrator',
        errors: []
      }
    }
  }

   getDetails (errors) {
    return _.map(Object.keys(errors), key => {
      return {
        key: errors[key].param,
        value: errors[key].value,
        message: errors[key].msg
      }
    })
  }

  getError (statusCode, errors = [], customMessage) {
    let responseError = this.errors[statusCode]
    if (!_.isEmpty(errors)) responseError.errors = this.getDetails(errors)
    if (!_.isEmpty(customMessage)) responseError.message = customMessage
    return responseError
  }
}

module.exports = ErrorUtil