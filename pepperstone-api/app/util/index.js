'use strict'

const ErrorUtils = require('./error')

class Util {
  constructor () {
    this.errors = new ErrorUtils()
  }
}

module.exports = Util