'use strict'

const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  account: {
    type: Number,
    required: true,
    unique: true
  },
  balance: {
    type: Number,
    required: true
  },
  currency: {
    type: String,
    required: true,
    trim: true,
    enum: ['AUD', 'USD', 'PHP']
  }
})

module.exports = {
  model: 'Account',
  collection: 'accounts',
  schema
}