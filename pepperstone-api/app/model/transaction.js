'use strict'

const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  type: {
    type: String,
    required: true
  },
  fileName: {
    type: String,
    required: true
  },
  fileId: {
    type: String,
    required: true
  },
  responseFileId: {
    type: String,
    default: ''
  },
  csv: [ // content of csv 
    {type: mongoose.Schema.Types.Mixed}
  ],
  createdDate: {
    type: Date,
    default: Date.now
  },
  processedDate: {
    type: Date
  }
})

module.exports = {
  model: 'Transaction',
  collection: 'transactions',
  schema
}