'use strict'

const _ = require('lodash')
const account = require('./account')
const transaction = require('./transaction')
const mongoose = require('mongoose')

// pre populated list of accounts
let accountList = [
  {
    account: 198000,
    balance: 1000000,
    currency: 'AUD'
  },
  {
    account: 123599,
    balance: 1000000,
    currency: 'AUD'
  },
  {
    account: 200198,
    balance: 1000000,
    currency: 'USD'
  },
  {
    account: 300210,
    balance: 1000000,
    currency: 'AUD'
  },
  {
    account: 110210,
    balance: 1000000,
    currency: 'PHP'
  }
]

class Model {

  async register () {
    let model = {
      account: mongoose.model(account.model, account.schema, account.collection),
      transaction: mongoose.model(transaction.model, transaction.schema, transaction.collection)
    }
    
    let accounts = await model.account.find({})

    // pre-populate accounts if collection is empty
    if (_.isEmpty(accounts)) {
      await model.account.insertMany(accountList)
    }
    
    return model
  }
}

module.exports = Model