'use strict'

const fs = require('fs')
const _ = require('lodash')

const Chance = require('chance')
const chance = new Chance()
const csv = require('csvtojson')
const json2csv = require('json2csv').Parser

class TransferService {
  constructor (logger) {
    this.type = 'transfer'
    this.logger = logger
    this.headers = ['From_Account', 'To_Account', 'Amount', 'Currency', 'Comment']
    this.resHeaders = ['From_Account', 'To_Account', 'Amount', 'Currency', 'Comment', 'Status', 'Error_Details']
    this.headerTypes = [Number, Number, Number, String, String, String, String]
  }

  async read (path) {
    try {
      let json = await csv().fromFile(path)
      return json
    } catch (err) {
      throw new Error(`Error parsing csv file due to: ${err.message}`)
    }
  }

  async sanitise (csv) {
    this.headers.forEach((header, i) => {
      csv.forEach((row) => {
        row.Error_Details = []
        let type = this.headerTypes[i]
        row[header] = row[header].trim()

        if (type === Number) {
          if (_.isNaN(Number(row[header]))) {
            row.Error_Details.push(`Invalid Value Type: ${header}, must be a Number`)
          } else {
            row[header] = Number(row[header])
          }
        }
      })
    })

    return csv
  }

  async process (model) {
    // find all unprocess documents

    let transfers = await model.transaction.find({
      type: this.type,
      processedDate: {
        $exists: false
      }
    })
    try {
      for (let transfer of transfers) {
        let csv = await this.validate(transfer.csv, model)
        csv = await this.calculate(transfer.csv, model)
        let responseFileId = await this.createResponse(csv)
        let doc = await model.transaction.findById(transfer._id).exec()
        
        doc.set({processedDate: new Date(), csv, responseFileId})
        transfer = await doc.save()
      }
    } catch (err) {
      throw new Error(err.message)
    }
    
    return transfers
  }

  async calculate (csv, model) {
    for (let row of csv) {
      let {From_Account, To_Account, Amount, errors} = row
      
      if (_.isEmpty(errors)) {
        let fAccount = await model.account.findOne({account: From_Account}).exec()
        let tAccount = await model.account.findOne({account: To_Account}).exec()

        if (fAccount.balance >= Amount) {
          fAccount.set({balance: fAccount.balance - Amount})
          tAccount.set({balance: tAccount.balance + Amount})
          await fAccount.save()
          await tAccount.save()
          
        } else {
          row.Error_Details.push(`Account '${fAccount.account}': Insufficient funds`)
        }
      }

      if (_.isEmpty(row.Error_Details)) {
        row.Status = 'Successful'
      } else {
        row.Status = 'Error'
      }
    }

    return csv
  }

  async validate (csv, model) {
    for (let row of csv) {
      let {From_Account, To_Account} = row

      let errors = []
      if (!_.isNumber(From_Account) || !_.isNumber(To_Account)) {
        if (!_.isNumber(From_Account)) errors.push(`From_Account '${From_Account}' does not exist`)
        if (!_.isNumber(To_Account)) errors.push(`To_Account '${From_Account}' does not exist`)
      } else {
        let fAccount = await model.account.findOne({
          account: From_Account
        })
        let tAccount = await model.account.findOne({
          account: To_Account
        })
        if (_.isNull(fAccount)) errors.push(`From_Account '${From_Account}' does not exist`)
        if (_.isNull(tAccount)) errors.push(`To_Account '${From_Account}' does not exist`)
        
        if (!_.isNull(fAccount) && !_.isNull(tAccount)) {
          if (fAccount.currency !== tAccount.currency) {
            errors.push(`Invalid currency in account ${To_Account}`)
          }
        }
      }
      row.Error_Details = errors
    }
    return csv
  }

  async save (data, model) {
    let transfer = model(data)
    try {
      transfer = await transfer.save()
    } catch (err) {
      throw new Error(`Error Creating Transfer: ${err.message}`)
    }
    
    return transfer
  }

  async createResponse (csv) {
    for (let row of csv) {
      row.Error_Details = row.Error_Details.join(', ')
    }
     const responseFileId = chance.guid()
    const parser = new json2csv({ fields: this.resHeaders})
    const csvResponse = parser.parse(csv)
    const filePath = `${appRoot}/csv/${responseFileId}`
    fs.closeSync(fs.openSync(filePath, 'w'));
    await fs.writeFile(filePath, csvResponse)
    return responseFileId
  }

}

module.exports = TransferService