'use strict'

const TransferService = require('./transfer')

class Services {
  constructor (logger, model) {
    this.logger = logger
    this.transfer = new TransferService(logger)
  }

  // get all types
  getTypes () {
    return [
      this.transfer.type,
      'adjustment'
    ]
  }
}

module.exports = Services