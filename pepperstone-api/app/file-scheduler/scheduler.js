'use strict'

const _ = require('lodash')
const cron = require('node-cron')

class Scheduler {
  constructor (services) {
    this.services = services
    this.schedule = process.env.SCHEDULE
  }

  async scheduleJob (model) {
    this.services.logger.info(`Scheduling File Processer: ${this.schedule}`)
    cron.schedule(this.schedule, () => {
      this.run(model)
    })
  }

  // process all queued transactions
  async run (model) {
    this.services.logger.info(`Running Schedule Job`)
    for (const type of this.services.getTypes()) {
      if (!_.isNil(this.services[type])) {
        let processFiles = await this.services[type].process(model)
        this.services.logger.info(`${processFiles.length} Files was processed`)
      }
    }
  }
}

module.exports = Scheduler