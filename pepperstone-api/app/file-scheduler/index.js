'use strict'

const Scheduler = require('./scheduler')

class FileScheduler {
  constructor (services, model) {
    this.services = services
    this.scheduler = new Scheduler(services, model)
  }

  async bootstrap (model) {
    await this.scheduler.scheduleJob(model)
  }
}

module.exports = FileScheduler