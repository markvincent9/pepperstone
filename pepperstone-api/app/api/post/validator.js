'use strict'

const _ = require('lodash')
const { body, header, validationResult } = require('express-validator/check')

class Validator {
  constructor (model, services, util) {
    this.model = model
    this.services = services
    this.util = util
  }

  sanitise() {
    return async (req, res, next) => {
      if (_.isNil(this.services[req.body.type])) {
        const errorDetails = this.util.errors.getError(501)
        res.status(errorDetails.status).json(errorDetails)
        next(false)
        
      } else if (!_.isNil(req.body.type) && !_.isNil(req.file)) {
        req.body = _.omit(req.body, 'csv')
        let csv = await this.services[req.body.type].read(req.file.path)
        csv = await this.services[req.body.type].sanitise(csv)
        req.body.csv = csv
        next()
        
      } else {
        next()
      }
    }
  }

  validate () {
    return [
      body('type')
        .trim()
        .exists().withMessage('Type is required')
        .custom(value => {
          const availTypes = this.services.getTypes()
          if (_.includes(availTypes, value)) return Promise.resolve(true)
          return Promise.reject(new Error(`Invalid Upload Type '${value}' must be in ${availTypes.join(', ')}`))
        }),
      body('csv')
        .exists().withMessage('File is required'),
      header('content-type')
        .trim()
        .exists().withMessage('Content Type Header is required')
        .custom(value => {
          if (_.includes(value, 'multipart/form-data')) return Promise.resolve(true)
          return Promise.reject(new Error(`Invalid Content Type: ${value}`))
        })
    ]
  }

  
  checkValidationResult () {
    return async (req, res, next) => {
      let errors = validationResult(req)
      if (errors.isEmpty()) {
        next()
      } else {
        let errorDetails = this.util.errors.getError(400, errors.mapped())
        res.status(errorDetails.status).json(errorDetails)
        next(false)
      }
    }
  }
}

module.exports = Validator