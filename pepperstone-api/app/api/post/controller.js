'use strict'

const _ = require('lodash')
const path = require('path')

class Controller {
  constructor (model, services, util) {
    this.model = model
    this.services = services
    this.util = util
  }

  upload () {
    return async (req, res, next) => {
      const {file} = req
      const {type, csv} = req.body
      try {
        const data = await this.services[type].save({
          type,
          csv,
          fileId: path.basename(file.path),
          fileName: file.originalname
        }, this.model.transaction)

        res.status(201).json(data)
        next(false)
      } catch (err) {
        const errorDetails = this.util.errors.getError(500, [], err.message)
        res.status(errorDetails.status).json(errorDetails)
        next(false)
      }
    }
  }
}

module.exports = Controller