'use strict'

const path = require('path')
const multer = require('multer')
const Validator = require('./validator')
const Controller = require('./controller')

const multerOptions = {
  dest: `${appRoot}/csv`,
  fileFilter: (req, file, cb) => { 
    let ext = path.extname(file.originalname)
    if (ext !== '.csv') {
      cb(new Error('Only text/csv files are allowed'))
    } else {
      cb(null, true)
    }
  },
  limits:{
    fileSize: 50 * 1024 // 50 KB limit
  }
}

const upload = multer(multerOptions).single('file')

class Router {
  constructor (model, services, httpServer, util) {
    this.controller = new Controller(model, services, util)
    this.validator = new Validator(model, services, util)
    this.httpServer = httpServer
    this.services = services
    this.util = util
  }

  async bootstrap () {
    this.httpServer.post('/transactions',
      (req, res, next) => {
        upload(req, res, (err) => {
          if (err) {
            let errorDetails = this.util.errors.getError(400, [], err.message)
            res.status(errorDetails.status).json(errorDetails)
            next(false)
          } else {
            next()
          }
        })
      },
      this.validator.sanitise(),
      this.validator.validate(),
      this.validator.checkValidationResult(),
      this.controller.upload()
    )

    return
  }
}

module.exports = Router