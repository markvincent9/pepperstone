'use strict'

const Controller = require('./controller')

class Router {
  constructor (model, services, httpServer, util) {
    this.controller = new Controller(model, services, util)
    this.httpServer = httpServer
    this.services = services
    this.util = util
  }

  async bootstrap () {
    this.httpServer.get('/transactions',
      this.controller.query()
    )

    this.httpServer.get('/transactions/download/:id',
      this.controller.download()
    )

    this.httpServer.get('/transactions/response/:id',
      this.controller.downloadResponse()
    )
  }
}

module.exports = Router