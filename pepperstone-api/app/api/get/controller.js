'use strict'

const _ = require('lodash')
const fs = require('fs')

class Controller {
  constructor (model, services, util) {
    this.model = model
    this.services = services
    this.util = util
  }

  query () {
    return async (req, res, next) => {
      let transactions = await this.model.transaction.find({}).sort({createdDate: -1})
      res.status(200).json(transactions)
      next(false)
    }
  }

  download () {
    return async (req, res, next) => {
      let {id} = req.params
      let transaction = await this.model.transaction.findById(id)
      
      if (!transaction) {
        const errorDetails = this.util.errors.getError(404)
        res.status(errorDetails.status).json(errorDetails)
        next(false)
      } else {
        res.setHeader('Content-disposition', `attachment; filename=${transaction.fileName}`)
        res.setHeader('Content-type', 'text/csv')
        fs.createReadStream(`${appRoot}/csv/${transaction.fileId}`).pipe(res)
      }
    }
  }

  downloadResponse () {
    return async (req, res, next) => {
      let {id} = req.params
      let transaction = await this.model.transaction.findOne({responseFileId: id})
      
      if (!transaction) {
        const errorDetails = this.util.errors.getError(404)
        res.status(errorDetails.status).json(errorDetails)
        next(false)
      } else {
        res.setHeader('Content-disposition', `attachment; filename=response-${transaction.fileName}`)
        res.setHeader('Content-type', 'text/csv')
        fs.createReadStream(`${appRoot}/csv/${transaction.responseFileId}`).pipe(res)
      }
    }
  }
}

module.exports = Controller