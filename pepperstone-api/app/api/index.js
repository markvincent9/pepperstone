'use strict'

const GetRoute = require('./get')
const PostRoute = require('./post')

class Api {
  constructor (httpServer, services, util) {
    this.httpServer = httpServer
    this.services = services
    this.util = util
  }

  async bootstrap (model) {
    // add more routes to array
    let routes = [
      new GetRoute(model, this.services, this.httpServer, this.util),
      new PostRoute(model, this.services, this.httpServer, this.util)
    ]

    for (const route of routes) {
      await route.bootstrap()
    }
  }
}

module.exports = Api