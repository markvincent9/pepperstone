'use strict'

const PATHS = {
  js: ['*.js', '*/*.js', '*/**/*.js', '!node_modules/**', '!gulpfile.js'],
  tests: ['tests/*.test.js']
}

const gulp = require('gulp')
const env = require('gulp-env')
const mocha = require('gulp-mocha')
const nodemon = require('gulp-nodemon')
const plumber = require('gulp-plumber')

gulp.task('run-tests', function () {
  require('dotenv').config()

  env({
    vars: { NODE_ENV: 'test' }
  })

  return gulp.src(PATHS.tests)
    .pipe(plumber())
    .pipe(mocha({
      reporter: 'spec',
      exit: true
    }))
})

let runApp = function () {
  require('dotenv').config()

  env({
    vars: { NODE_ENV: 'test' }
  })

  nodemon({
    script: 'main.js',
    ext: 'js',
    watch: PATHS.js,
    ignore: [
      'node_modules',
      '.git',
      '.gitignore',
      'Dockerfile',
      '.dockerignore',
    ],
    restartable: 'rs'
  })
}

gulp.task('run', () => runApp())
gulp.task('test', ['run-tests'])
