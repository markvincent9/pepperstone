# Pepperstone Exam

### Prerequisite
install [docker](https://docs.docker.com/install/#supported-platforms)
install [docker-compose](https://docs.docker.com/compose/install/#install-compose)
### How to run using 'docker-compose'
Steps to run:
- Install docker-compose
- Go to Project Root Directory
- Run '**docker-compose up**'
- Make sure PORT *8080* and *3000* are open

### How to run locally
- RUN '**docker run --name mongo -p 27017:27017 -d mongo**' to run a mongo container
- To run Express Server, Go to '**pepperstone-api**' dir and RUN '**npm install && gulp run**'
- To run Web App, Go to '**pepperstone-portal**' dir and RUN '**npm install && npm start**'

### How to run tests
- Go to '**pepperstone-api**' and RUN **gulp test**
- test csv file is in /pepperstone-api/tests/csv-files/test1.csv

