import React, { Component } from 'react';

import NavBar from './navbar';
import Uploader from './uploader';
import TransactionList from './transaction_list'

export default class App extends Component {
  render() {
    return (
      <div>
        <NavBar></NavBar>
        <Uploader></Uploader>
      </div>
    )
  }
}
