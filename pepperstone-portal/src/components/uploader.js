import React, { Component } from 'react'
import axios from 'axios'

import TransactionList from './transaction_list'

const URL = 'http://localhost:8080/transactions'
const types = [
  {
    code: 'transfer',
    name: 'Cash Transfer'
  },
  {
    code: 'adjustment',
    name: 'Cash Adjustment'
  }
]

export default class Uploader extends Component {
  constructor (props) {
    super(props)

    this.state = {
      error: '',
      selectedType: types[0].code,
      transactions: []
    }

    this.upload = this.upload.bind(this)
    this.refreshTransactions = this.refreshTransactions.bind(this)
    this.refreshTransactions()
    this.startPoll() //refresh every 5 sec
    
  }

  render() {
    return (
      <div>
        <div className="row justify-content-center">
          <div className="col-8 mt-4">
            <form onSubmit={this.upload}>
            <div className="card">
              <div className="card-body">
                <div className="custom-file">
                  <input type="file" className="custom-file-input" id="customFile" ref={(ref) => { this.selectedFile = ref }}/>
                  <label className="custom-file-label" htmlFor="customFile">Choose file</label>
                </div>
                <div className="dropdown mt-2">
                  <button className="btn btn-primary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Select Type
                  </button>
                  <div className="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    {this.getTypes()}
                  </div>
                  <span> type: {this.state.selectedType}</span>
                  <span className={this.state.error ? '' : 'd-none'}></span><button type="submit" className="btn btn-primary float-right">Upload</button>
                </div>
              </div>
            </div>
            </form>
          </div>
        </div>
        <TransactionList transactions = {this.state.transactions}></TransactionList>
      </div>
    )
  }

  onSelectType (type) {
    this.setState({
      selectedType: type
    })
  }

  getTypes () {
    return types.map(type => {
      return (<a className="dropdown-item" onClick={() => this.onSelectType(type.code)} key={type.code}>{type.name}</a>)
    })
  }

  upload (ev) {
    ev.preventDefault()
    const data = new FormData()
    data.append('file', this.selectedFile.files[0])
    data.append('type', this.state.selectedType)

    axios.post(URL, data)
      .then(() => this.refreshTransactions())
      .catch(err => {
        alert(`Error Uploading: ${err.response.data.message}`)
      })
  }

  refreshTransactions () {
    fetch(URL)
    .then(res => res.json())
    .then(data => {
      this.setState({
        error: this.state.error,
        selectedType: this.state.selectedType,
        transactions: data
      })
    })
  }

  startPoll () {
    setInterval (this.refreshTransactions, 5000)
  }
}
