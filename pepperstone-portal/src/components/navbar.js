import React, { Component } from 'react';

export default class NavBar extends Component {
  render() {
    return (
      <nav className="navbar navbar-fixed-top navbar-light bg-light">
        <img className="navbar-brand" src="https://www.forexbrokerz.com/files/1457528556Pepperstone-500.png" alt="logo" height="80px" width="150px"></img>
      </nav>
    );
  }
}
