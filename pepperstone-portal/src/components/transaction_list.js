import React, { Component } from 'react'

export default class TransactionList extends Component {
  render() {
    return (
      <div className="row justify-content-center">
        <div className="col-11 mt-5">
        <table className="table">
          <thead className="thead-dark">
            <tr>
              <th scope="col">File Name</th>
              <th scope="col">Type</th>
              <th scope="col">Upload Date</th>
              <th scope="col">Status</th>
              <th scope="col">Completion Date</th>
              <th scope="col">Response</th>
            </tr>
          </thead>
          <tbody>
            {this.getRows()}
          </tbody>
        </table>
        </div>
      </div>
    );
  }

  getRows () {
    const transactions = this.props.transactions
    return transactions.map(transaction => {
      return (
        <tr key={transaction._id}>
          <th scope="row">{this.getDownloadUrl(transaction)}</th>
          <td>{transaction.type}</td>
          <td>{transaction.createdDate}</td>
          <td>{transaction.processedDate ? 'Successful' : 'Pending'}</td>
          <td>{transaction.processedDate ? transaction.processedDate : ''}</td>
          <td>{this.getResponseUrl(transaction)}</td>
        </tr>
      )
    })
  }

  getDownloadUrl (transaction) {
    return <a href={`http://localhost:8080/transactions/download/${transaction._id}`}>{transaction.fileName}</a>
  }

  getResponseUrl (transaction) {
    return transaction.responseFileId ? <a href={`http://localhost:8080/transactions/response/${transaction.responseFileId}`}>link</a> : ''
  }
}